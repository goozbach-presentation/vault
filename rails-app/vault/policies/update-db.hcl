path "auth/token/lookup-self" {
  capabilities = ["read", "list"]
}

path "secret/*" {
  capabilities = ["create", "update"]
  allowed_parameters = {
    "DATABASE_PASSWORD" = []
    "DATABASE_USER"   = []
  }
}
