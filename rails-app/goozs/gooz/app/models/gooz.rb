class Gooz < ApplicationRecord
        def goozvalue=(string)
                # do my vault stuff
                secret = Vault.logical.write("transit/encrypt/goozs", plaintext: Base64.strict_encode64(string))
                enc_value = secret.data[:ciphertext]
                super(enc_value)
        end
        def decryptedvalue
                secret = Vault.logical.write("transit/decrypt/goozs", ciphertext: goozvalue)
                dec_value = Base64.strict_decode64 secret.data[:plaintext]
        end
end
