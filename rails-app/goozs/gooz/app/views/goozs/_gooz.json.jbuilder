json.extract! gooz, :id, :goozname, :goozvalue, :created_at, :updated_at
json.url gooz_url(gooz, format: :json)
