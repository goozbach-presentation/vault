class GoozsController < ApplicationController
  before_action :set_gooz, only: [:show, :edit, :update, :destroy]

  # GET /goozs
  # GET /goozs.json
  def index
    @goozs = Gooz.all
  end

  # GET /goozs/1
  # GET /goozs/1.json
  def show
  end

  # GET /goozs/new
  def new
    @gooz = Gooz.new
  end

  # GET /goozs/1/edit
  def edit
  end

  # POST /goozs
  # POST /goozs.json
  def create
    @gooz = Gooz.new(gooz_params)

    respond_to do |format|
      if @gooz.save
        format.html { redirect_to @gooz, notice: 'Gooz was successfully created.' }
        format.json { render :show, status: :created, location: @gooz }
      else
        format.html { render :new }
        format.json { render json: @gooz.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goozs/1
  # PATCH/PUT /goozs/1.json
  def update
    respond_to do |format|
      if @gooz.update(gooz_params)
        format.html { redirect_to @gooz, notice: 'Gooz was successfully updated.' }
        format.json { render :show, status: :ok, location: @gooz }
      else
        format.html { render :edit }
        format.json { render json: @gooz.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goozs/1
  # DELETE /goozs/1.json
  def destroy
    @gooz.destroy
    respond_to do |format|
      format.html { redirect_to goozs_url, notice: 'Gooz was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gooz
      @gooz = Gooz.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gooz_params
      params.require(:gooz).permit(:goozname, :goozvalue)
    end
end
