class CreateGoozs < ActiveRecord::Migration[5.0]
  def change
    create_table :goozs do |t|
      t.string :goozname
      t.string :goozvalue

      t.timestamps
    end
  end
end
