require 'test_helper'

class GoozsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gooz = goozs(:one)
  end

  test "should get index" do
    get goozs_url
    assert_response :success
  end

  test "should get new" do
    get new_gooz_url
    assert_response :success
  end

  test "should create gooz" do
    assert_difference('Gooz.count') do
      post goozs_url, params: { gooz: { goozname: @gooz.goozname, goozvalue: @gooz.goozvalue } }
    end

    assert_redirected_to gooz_url(Gooz.last)
  end

  test "should show gooz" do
    get gooz_url(@gooz)
    assert_response :success
  end

  test "should get edit" do
    get edit_gooz_url(@gooz)
    assert_response :success
  end

  test "should update gooz" do
    patch gooz_url(@gooz), params: { gooz: { goozname: @gooz.goozname, goozvalue: @gooz.goozvalue } }
    assert_redirected_to gooz_url(@gooz)
  end

  test "should destroy gooz" do
    assert_difference('Gooz.count', -1) do
      delete gooz_url(@gooz)
    end

    assert_redirected_to goozs_url
  end
end
